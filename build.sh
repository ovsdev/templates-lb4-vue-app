#!/bin/bash

## a gloabal variable DOCKER_IMAGE_TAG is setup during the build process
## use this value to build the app docker image 

set -x

## build the ui vue application
cd ./ui
npm install
npm run build



## build the Lb4 server app
cd ../server
npm install


## move the vue build artifacts into the exposed folder of the server app
rm -fr ./public
UI_HOME=../ui
UI_DIST="$UI_HOME/dist"

mkdir public
mkdir public/js
mkdir public/css
mkdir public/img

cp -rf "$UI_DIST/js/"*.js public/js
cp -rf "$UI_DIST/css/"*.css public/css
cp -rf "$UI_DIST/img/"* public/img
cp -rf "$UI_DIST/index.html" public
cp -rf "$UI_DIST/favicon.ico" public


## build the application docker image
docker build --network=host -t $DOCKER_IMAGE_TAG . 